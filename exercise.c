#include <stdio.h>

int main(){
    int price, qty ,allCost, allIncome, allProfit;
    do{
        printf("Enter the Price : ");
        scanf("%d",&price);
        qty = quantity(price);
        printf("Quantity of People is %d\n",qty);
        allIncome = income(price,qty);
        printf("Income is %d\n",allIncome);
        allCost = cost(qty);
        printf("cost is %d\n",allCost);
        allProfit = profit(allCost, allIncome);
        printf("profit is %d\n",allProfit);

        printf("\nTo stop this loop enter \"0\"\n\n");

    }
    while(price != 0);
    return 0;
}

int quantity(int p){
    if(p==15){return 120;}
    else if(p==10){return 140;}
    else if(p==20){return 100;}
}

int cost(int q){
    return 500+(q*3);
}

int income(int p,int q){
    return p*q;
}

int profit(int c, int i){
    return i-c;
}
